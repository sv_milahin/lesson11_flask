from utils import get_candidate_skill


def test_get_candidate_skill():
    d = get_candidate_skill('python')
    assert d == ['Adela Hendricks', 'Austin Cochran', 'Sheree Love']

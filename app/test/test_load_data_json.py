from utils import load_data_json


def test_load_data_json():
    assert load_data_json()[0]['id'] == 1
    assert load_data_json()[0]['skills'] == 'go, python'
    assert load_data_json()[0]['age'] == 40



from utils import get_candidate_name


def test_get_candidate_name():
    assert get_candidate_name('She') == [(2, 'Sheri Torres'), (7, 'Sheree Love')]

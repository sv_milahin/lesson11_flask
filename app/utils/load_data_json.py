import json


def load_data_json() -> list:
    with open('data/candidates.json', encoding='UTF-8') as fl:
        data = json.load(fl)
    return data


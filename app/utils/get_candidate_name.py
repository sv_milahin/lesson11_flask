from utils import load_data_json


def get_candidate_name(candidate_name: str) -> list:
    data = load_data_json()
    return [(name['id'], name['name']) for name in data 
            if candidate_name.lower() in name['name'].lower()
            ]

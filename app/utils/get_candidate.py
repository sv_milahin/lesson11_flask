from utils import load_data_json


def get_candidate_id(candidate_id: int) -> list:
   data = load_data_json()
   return [i for i in data
           if i['id'] == candidate_id
           ]

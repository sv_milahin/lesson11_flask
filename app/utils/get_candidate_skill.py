from utils import load_data_json


def get_candidate_skill(skill_name):
    data = load_data_json()
    return [(skill['id'], skill['name']) for skill in data 
            if  skill_name.lower() in skill['skills'].lower().split(', ')
            ]

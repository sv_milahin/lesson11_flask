from flask import Flask, render_template, request

from dotenv import load_dotenv

from utils import get_candidate_all
from utils import get_candidate_id
from utils import get_candidate_name
from utils import get_candidate_skill


load_dotenv('./.flaskenv')


app = Flask(__name__)


@app.route('/')
def index() -> str:
    content: list = get_candidate_all() 
    return render_template('index.html', content=content)


@app.route('/candidate/<int:x>')
def candidate(x: int) -> list:
    
    content: list = get_candidate_id(x)
    
    return render_template('card.html', content=content)


@app.route('/search/<name>')
def search(name) -> list:


    content: list = get_candidate_name(name)
    
    return render_template('search.html', content=content)


@app.route('/skill/<skill_name>')
def skill(skill_name) -> list:


    content: list = get_candidate_skill(skill_name)
    
    return render_template('skill.html', content=content, skill_name=skill_name)


if __name__ == '__main__':
    app.run()
